<?
require __DIR__ . '/../config.php';

// error_reporting(-1);
// ini_set('display_errors', 1);

function autoload ($class) {	
	$file = __DIR__ . '/../classes/' . str_replace("\\", "/", $class) . '.php';  
	if(file_exists($file)) {
		require_once $file;		
	}
}

spl_autoload_register("autoload");
