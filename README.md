## Service of monitoring

#### by true.code 2022

<br>
<hr>

### The service contains functionality for:

1. creating database table for monitoring;
2. adding and editing domains;
3. updating domains access data (exire date for domain/ssl and http-status);
4. api for updating domains data (e.g by cron);
5. sending alerts to telegram chat;

<br>
<hr>

### The listed functionality is provided by classes:

<br>

_*DataBase*_ - methods for work with database.

_*WhoisApi*_ - methods for work with whois and extra apis to get and format domain/ssl/access data.

_*TelegramBot*_ - methods for connecting bot and sending messages.

_*Utils*_ - extra auxiliary methods.

<br>
<hr>

### For cron:

<br>

The file _api_update.php_ updates domains data or access erorrs by getting $\_GET parameters.

Also the file _api_check.php_ checks exipre dates or amount of access erorrs and sends messages to telegram chats by chat id.
