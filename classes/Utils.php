<?php

/**
* Utils
*/
class Utils {
    
    /**
    * __construct        
    */
    public function __construct () {                       
    }


    /**
    * isToday        
    */
    public function isToday (string $date) {  
        $today = new DateTime;
        $date = new DateTime($date);
        $today->setTime( 0, 0, 0 );
        $date->setTime( 0, 0, 0 );
      
        return $today->diff($date)->days === 0;
    }

    /**
    * daysToDate        
    */
    public function daysToDate(string $date) {
        $now = date('Y-m-d', time());
        $seconds = abs(strtotime($date) - strtotime($now));
        return round($seconds / 86400, 1); 
    }

}