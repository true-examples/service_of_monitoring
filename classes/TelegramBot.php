<? 


/**
* TelegramBot
*/
class TelegramBot {
    /**
    * BOT_TOKEN 
    * @var string $BOT_TOKEN
    */
    private $BOT_TOKEN;


		/**
    * API_LINK 
    * @var string $API_LINK
    */
    private $API_LINK;


    /**
    * __construct
    */
    public function __construct ($BOT_TOKEN) {
        $this->BOT_TOKEN = $BOT_TOKEN;  
				$this->API_LINK = 'https://api.telegram.org/bot'. $BOT_TOKEN;   
    }


		/**
    * initConnection 
    */  
    public function initConnection () {				
			$bot_url = 'https://api.telegram.org/bot'.$this->BOT_TOKEN.'/getMe';			
      $bot_content = file_get_contents($bot_url);
      $bot_data = json_decode($bot_content, true);

			if (!$bot_data['ok']){  
        return false;				
				
			} else {				
        $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";    
		    $webhook_url = 'https://api.telegram.org/bot'.$this->BOT_TOKEN.'/setWebhook?url='.$actual_link;
		    $get_webhook = file_get_contents($webhook_url);								
        return true;          				 
			}
    }


		/**
    * sendMessage 
    */  
		public function sendMessage ($chat_id, $message) {
			file_get_contents($this->API_LINK. '/sendMessage?chat_id=' . $chat_id . '&text=' . urlencode($message));
		}



    /**
    * sendAlertToChats   
    */  
		public function sendAlertToChats (string $alert_type, array $domain_data) {
         
      $chat_ids = explode(',', $domain_data['chat_ids']);
      
      if (count($chat_ids) > 0) {

        $alert_message = $this->formatAlertMessage($alert_type, $domain_data);

        foreach($chat_ids as $chat_id) {
          $chat_id = trim($chat_id);                    
          $this->sendMessage($chat_id, $alert_message);
        }
      }
		}


    /**
    * formatAlertMessage   
    */  
    private function formatAlertMessage (string $alert_type, array $domain_data) {

      if ($alert_type == 'domain' || $alert_type == 'ssl') {
        $now = date('Y-m-d', time());
        $seconds = abs(strtotime($domain_data['domain_expire_date']) - strtotime($now));
        $days_to_expire = round($seconds / 86400, 1);
        $symbol = "🆗"; 

        if ($days_to_expire <= 7) $symbol = "🔥";      
        if ($days_to_expire == 14) $symbol = "‼️";      
        if ($days_to_expire == 21) $symbol = "❗️";      
        if ($days_to_expire == 28) $symbol = "⚠️";
      }

      $now = date('d-m-Y', time());

      switch ($alert_type) {
        case 'first-access-error':
          $alert_message = "--".$now."-- \n 📄 Доступность сайта \n Сайт: ".$domain_data['site_name']." \n Домен: ".$domain_data['domain']." \n \n 🔥 Сайт недоступен.\n Детали: ".$domain_data['access_error_code']."."; 
          break;

        case 'next-access-error':
          $alert_message = "--".$now."-- \n 📄 Доступность сайта \n Сайт: ".$domain_data['site_name']." \n Домен: ".$domain_data['domain']." \n \n 🔥 Сайт все ещё недоступен.\n Детали: ".$domain_data['access_error_code'].". ";
          break;

        case 'access-resolve':
          $alert_message = "--".$now."-- \n 📄 Доступность сайта \n Сайт: ".$domain_data['site_name']." \n Домен: ".$domain_data['domain']." \n \n ☀️Работа сайта восстановлена."; 
          break;

        case 'domain':                       
          $alert_message = "--".$now."-- \n 🏠 Проверка регистрации домена \n Сайт: ".$domain_data['site_name']." \n Домен: ".$domain_data['domain']." \n Хостинг: ".$domain_data['hoster']." \n \n ".$symbol." Информация: Регистрация домена истекает через ".$days_to_expire." дней. \n Дата истечения: ".$domain_data['domain_expire_date'].".";  
          break; 

        case 'ssl': 
          $alert_message = "--".$now."-- \n 📄 Проверка SSL сертификата \n Сайт: ".$domain_data['site_name']." \n Домен: ".$domain_data['domain']." \n SSL выдан: ".$domain_data['ssl_issuer']." \n \n ".$symbol." Информация: Сертификат для домена истекает через ".$days_to_expire." дней. \n Дата истечения: ".$domain_data['ssl_expire_date'].".";
          break; 

        default:            
          break;
      } 

      return $alert_message;
    }



    /**
    * sendMessageHTML  
    */  
		public function sendMessageHTML ($chat_id, $message) {
      $parameters = [
          'text'       => $message,
          'chat_id'    => $chat_id,
          'parse_mode' => 'HTML'
      ];
			file_get_contents($this->API_LINK. '/sendMessage?'.http_build_query($parameters));            
		}



		/**
    * getPostData 
    */  
		public function getPostData () {			
			$output = json_decode(file_get_contents('php://input'), TRUE); 			
			
			$chat_id = $output['message']['chat']['id'];
			$message = $output['message']['text']; 

			return $output;
		}

}
?>