<?php

/**
* DataBase
*/
class DataBase {

    /**
    * DB_HOST, DB_USER, DB_PASS, DB_NAME
    * @var string $DB_HOST
    * @var string $DB_USER
    * @var string $DB_PASS
    * @var string $DB_NAME
    */
    private $DB_HOST;
    private $DB_USER;
    private $DB_PASS;
    private $DB_NAME;
  
    /**
    * LINK
    * @var mysqli_connect $LINK
    */
    private $LINK;

    /**
    * __construct
    */
    public function __construct (array $DB_ACCESS) {
        $this->DB_HOST = $DB_ACCESS["DB_HOST"];
        $this->DB_USER = $DB_ACCESS["DB_USER"];
        $this->DB_PASS = $DB_ACCESS["DB_PASS"];
        $this->DB_NAME = $DB_ACCESS["DB_NAME"];
    
        $this->LINK = mysqli_connect($this->DB_HOST, $this->DB_USER, $this->DB_PASS, $this->DB_NAME);
    }


    /**
    * createMonitoringTable
    */
    public function createMonitoringTable () {

        $sql = "SHOW TABLES LIKE 'monitoring'";
        $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));

        $table_created = $result['num_rows'];

        if (!$table_created) {
            $sql = "CREATE TABLE `monitoring` (
                `id` int NOT NULL AUTO_INCREMENT,
                `domain` varchar(100) NOT NULL,
                `site_name` varchar(250) NOT NULL,
                `domain_expire_date` date DEFAULT NULL,
                `hoster` varchar(100) DEFAULT NULL,
                `ssl_expire_date` date DEFAULT NULL,
                `ssl_issuer` varchar(250) DEFAULT NULL,
                `http_status` varchar(10) DEFAULT NULL,
                `user_id` int(11) NOT NULL,
                `chat_ids` varchar(500) DEFAULT NULL,
                `check_access` tinyint(4) DEFAULT NULL,
                `check_domain` tinyint(4) DEFAULT NULL,
                `check_ssl` tinyint(4) DEFAULT NULL,
                `updated` datetime DEFAULT NULL,
                `access_error_counter` int(11) DEFAULT '0',
                `access_error_code` varchar(15) DEFAULT NULL,
                `access_error_sent` tinyint(11) DEFAULT '0'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));

            return $result;
        }
    }    

        
    

    /**
    * addDomain 
    * by user interface
    */
    public function addDomain (array $domain_data) {

        if (!empty($domain_data)) {

            $domain = $domain_data['domain'];
            $site_name = $domain_data['site_name'];
            $user_id = $domain_data['user_id'];
            $chat_ids = $domain_data['chat_ids'];
            $check_access = $domain_data['check_access'] == 'on' ? 1 : 0;
            $check_domain = $domain_data['check_domain'] == 'on' ? 1 : 0;
            $check_ssl = $domain_data['check_ssl'] == 'on' ? 1 : 0;
    
            $sql = "INSERT INTO `monitoring`(`domain`, `site_name`, `user_id`, `chat_ids`, `check_access`, `check_domain`, `check_ssl`) VALUES ('$domain', '$site_name', '$user_id', '$chat_ids', '$check_access', '$check_domain', '$check_ssl')";
    
            $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));
    
            return $result;
        }

        return false;
    }


    /**     
    * updateDomainDataByUser 
    * by user interface
    */
    public function updateDomainDataByUser (array $domain_data) {

        if (!empty($domain_report)) { 

            $domain = $domain_data['domain'];
            $user_id = $domain_data['user_id'];

            $site_name = $domain_data['site_name'];
            $chat_ids = $domain_data['chat_ids'];
            $check_access = $domain_data['check_access'] == 'on' ? 1 : 0;
            $check_domain = $domain_data['check_domain'] == 'on' ? 1 : 0;
            $check_ssl = $domain_data['check_ssl'] == 'on' ? 1 : 0;                    

            $sql = "UPDATE monitoring SET site_name='$site_name', chat_ids='$chat_ids', check_access='$check_access', check_domain='$check_domain', check_ssl='$check_ssl' WHERE domain='$domain' AND user_id='$user_id'";

            $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));

            return $result;
        }   

        return false;
    }



    /**     
    * deleteDomain 
    * by user interface   
    */
    public function deleteDomain ($domain, $user_id) {

        $sql = "DELETE FROM `monitoring` WHERE `domain` = '$domain' AND `user_id` = '$user_id'";
        $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));

        return $result;
    }


    /**     
    * isUserAlreadyHasDomain  
    * by user interface
    */
    public function isUserAlreadyHasDomain ($domain, $user_id) {
        
        $sql = "SELECT * FROM `monitoring` WHERE `domain` = '$domain' AND `user_id` = $user_id";
        $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));

        for ($domain_data = []; $row = mysqli_fetch_assoc($result); $domain_data[] = $row);

        return !empty($domain_data);                      
    }


    /**     
    * getUserDomains 
    * by user interface   
    */
    public function getUserDomains (int $user_id) {
        $sql = "SELECT * FROM `monitoring` WHERE user_id = '$user_id'";
	    $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));

	    for ($domains_data = []; $row = mysqli_fetch_assoc($result); $domains_data[] = $row);

        return $domains_data;
    }



    /**     
    * updateDomainDataByReport
    */
    public function updateDomainDataByReport (array $domain_report) {

        if (!empty($domain_report)) {

            $domain = $domain_report['domain'];

            $domain_expire_date = $domain_report['domain_expire_date'];
            $domain_expire_date = date('Y-m-d', strtotime($domain_expire_date));

            $hoster = $domain_report['hoster'];
            $hoster = str_replace("'", '', $hoster);

            $ssl_issuer = $domain_report['ssl_issuer'];
            $ssl_issuer = str_replace("'", '', $ssl_issuer);

            $ssl_expire_date = $domain_report['ssl_expire_date'];
            $ssl_expire_date = date('Y-m-d', strtotime($ssl_expire_date));

            $http_status = $domain_report['http_status'];
            $updated = $domain_report['updated'];        

            $sql = "UPDATE monitoring SET domain_expire_date='$domain_expire_date', hoster='$hoster', ssl_issuer='$ssl_issuer', ssl_expire_date='$ssl_expire_date', http_status='$http_status', updated='$updated' WHERE domain='$domain'";

            $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));

            return $result;
        }   

        return false;
    }



    /**     
    * getLastUpdatedDomainData    
    */
    public function getLastUpdatedDomainData () {        
        $sql = "SELECT domain, updated FROM `monitoring` WHERE updated = (SELECT min(updated) FROM `monitoring`)";
	    $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));

	    for ($domain_data = []; $row = mysqli_fetch_assoc($result); $domain_data[] = $row);

        return $domain_data[0];
    }



    /**
    * getDomainsDataToCheck        
    */
    public function getDomainsDataToCheck (string $condition) {

        $sql = 'SELECT * FROM monitoring';       

        switch ($condition) {
            case 'all': 
                $sql = 'SELECT * FROM monitoring';  
                break;
            case 'access': 
                $sql = 'SELECT * FROM monitoring WHERE check_access = 1';
                break;
            case 'domain':            
                $sql = 'SELECT * FROM monitoring WHERE check_domain = 1';
                break;    
            case 'ssl':            
                $sql = 'SELECT * FROM monitoring WHERE check_ssl = 1';
                break;    
            default:  
                $sql = 'SELECT * FROM monitoring';     
                break;
        }  

        $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));
        for ($domains_data = []; $row = mysqli_fetch_assoc($result); $domains_data[] = $row);

        return $domains_data;
    }


    /**
    * manageAccessError        
    */
    public function manageAccessError ($domain, $error, $http_status) {

        if ($error == true) {
            $sql = "SELECT access_error_counter FROM monitoring WHERE domain='$domain'";
            $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));
            $error_counter = mysqli_fetch_assoc($result);
            $error_counter = (int)$error_counter['access_error_counter'];
            $error_counter = $error_counter + 1;
            
            $sql = "UPDATE monitoring SET access_error_counter='$error_counter', access_error_code='$http_status' WHERE domain='$domain'";
            $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));
        }

        if ($error == false) {
            $sql = "UPDATE monitoring SET access_error_counter=0, access_error_code='$http_status' WHERE domain='$domain'";
            $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));
        }

    }


    /**
    * setAlertMessageSent        
    */
    public function setAlertMessageSent ($domain, $status) {
        $status = $status ? 1 : 0;
        $sql = "UPDATE monitoring SET access_error_sent='$status' WHERE domain='$domain'";
        $result = mysqli_query($this->LINK, $sql) or die(mysqli_error($this->LINK));
    }


}