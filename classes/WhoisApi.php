<?php

/**
* WhoisApi
*/
class WhoisApi {

    /**
    * WHOIS_SERVERS     
    * @var array $WHOIS_SERVERS
    */
    private $WHOIS_SERVERS;


    /**
    * __construct        
    */
    public function __construct ($WHOIS_SERVERS) {  
        $this->WHOIS_SERVERS = $WHOIS_SERVERS;              
    }


    function hi (){
      echo 'hellO!';
    }


    /**
    * getDomainExpireDate  
    */
    public function getDomainExpireDate($domain) {   

      $domain = $this->convertDomainToWhoisFomat($domain);
              
      $domain_parts = explode(".", $domain);
      $tld = strtolower(array_pop($domain_parts));      

      $whoisserver = $this->WHOIS_SERVERS[$tld];        
      if (!$whoisserver) {
        return "Ошибка: Нет соответствующего whois-сервера для домена $domain!";
      }

      $result = $this->queryWhoisServer($whoisserver, $domain);
      if (!$result) {
        return "Ошибка: Нет результатов полученных от $whoisserver сервера для домена $domain!";
      }
      else {
        while (strpos($result, "Whois-сервер:") !== FALSE) {
          preg_match("/Whois Server: (.*)/", $result, $matches);
          $secondary = $matches[1];
          if ($secondary) {
            $result = $this->queryWhoisServer($secondary, $domain);
            $whoisserver = $secondary;
          }
        }

      }

      $expire_date = $this->domainExpireFormat($result, $domain);
      
      return $expire_date;
    }


    /**
    * getHoster  
    */
    public function getHoster($domain) {
        $json = file_get_contents('http://ip-api.com/json/' . $domain . '?lang=ru');
        $array = json_decode($json, TRUE);
              
        if ($array['org']) {
          return $array['org'];
        }
        if ($array['isp']) {
          return $array['isp'];
        }
      
        return 'x';
    }



    /**
    * getSSLExpireDateAndIssuer  
    */
    public function getSSLExpireDateAndIssuer ($domain) {
        
      $domain = $this->convertDomainToWhoisFomat($domain);

      $ssl_context = stream_context_create (array("ssl" => array("capture_peer_cert" => true)));
      $ssl_response = fopen('https://'.$domain, "rb", false, $ssl_context);      
      $cert_full = stream_context_get_params($ssl_response);
      $cert_info = openssl_x509_parse($cert_full['options']['ssl']['peer_certificate']);

      return [
        'ssl_issuer' => $cert_info ? $cert_info['issuer']['O'] : 'x',
        'ssl_expire_date' => $cert_info ? date('d.m.Y', $cert_info['validTo_time_t']) : 'x'
      ];

    }



    /**
    * getHttpStatus  
    */
    public function getHttpStatus($domain) {
        $user_agent = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $domain);
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $page = curl_exec($ch);
      
        $err = curl_error($ch);
        if (!empty($err))
        return $err;
      
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $httpcode;
    }



    /**
    * isValidPageSize  
    * ЗАПРОС К API, получение размера страницы
    */
    public function isValidPageSize ($domain) {
        $size = strlen( file_get_contents('http://' . $domain) );    
        if ($size > 5000) {
            return true;
        } else {
            return false;
        }
    }



    /**
    * fullDomainReport  
    */
    public function fullDomainReport ($domain) {
        
      $domain_expire_date = $this->getDomainExpireDate($domain);
    
      $ssl_data = $this->getSSLExpireDateAndIssuer($domain);  
      $ssl_issuer = $ssl_data['ssl_issuer'];
      $ssl_expire_date = $ssl_data['ssl_expire_date'];
      
      $hoster = $this->getHoster($domain);

      $http_status = $this->getHttpStatus($domain);

      $updated = date('Y.m.d H:i:s');
    
      $report_array = array(
        'domain' =>  $domain,
        'domain_expire_date' => $domain_expire_date,          
        'hoster' => $hoster,
        'ssl_issuer' => $ssl_issuer,
        'ssl_expire_date' => $ssl_expire_date,  
        'http_status' => $http_status,  				
        'updated' => $updated    				
      );
    
      return $report_array;
    }



    /**
    * queryWhoisServer        
    */
    private function queryWhoisServer($whoisserver, $domain) {
        $port = 43;
        $timeout = 10;
        $fp = @fsockopen($whoisserver, $port, $errno, $errstr, $timeout);
        if (!$fp) {
          return "Socket Error " . $errno . " - " . $errstr;
        }
        fputs($fp, $domain . "\r\n");
        $out = "";
        while(!feof($fp)){
          $out .= fgets($fp);
        }
        fclose($fp);
       
        $res = "";
        if((strpos(strtolower($out), "error") === FALSE) && (strpos(strtolower($out), "not allocated") === FALSE)) {
          $rows = explode("\n", $out);
          foreach($rows as $row) {
            $row = trim($row);
            if(($row != '') && ($row[0] != '#') && ($row[0] != '%')) {
              $res .= $row."\n";
            }
          }
        }
        return $res;
    }

    
    /**
    * convertDomainToWhoisFomat    
    */
    private function convertDomainToWhoisFomat ($domain) {
      $domain_parts = explode(".", $domain);
      $tld = strtolower(array_pop($domain_parts));

      if ($tld == 'рф') {
        $domain = idn_to_ascii($domain);
      }

      return $domain;
    }



    /**
    * domainExpireDaysFormat  
    */
    private function domainExpireFormat ($whois_result, $domain) {

      $domain_parts = explode(".", $domain);
      $tld = strtolower(array_pop($domain_parts));

      // default
      $exp_date = '01-01-1970';

      // $ru_region
      $ru_region = ["ru", "xn--p1ai", "su"];
      if (in_array($tld, $ru_region)) {
        $pos = strrpos($whois_result, "paid-till:");			
        $exp_date = mb_substr($whois_result, $pos + 11, 14); 
      }

      // $eu_region
      $eu_region = ['uk', 'com', 'net', 'shop', 'org', 'me', 'world', 'ie', 'site', 'live', 'online'];
      if (in_array($tld, $eu_region)) {
        $pos = strrpos($whois_result, "Expiry Date:");			
        $exp_date = mb_substr($whois_result, $pos + 13, 10); 
      }

      // cz_region 
      if ($tld == 'cz') {
        $pos = strrpos($whois_result, "expire:");			
        $exp_date = mb_substr($whois_result, $pos + 13, 12); 
      }

      // nz_region 
      if ($tld == 'nz') {
        $pos = strrpos($whois_result, "Updated Date:");			
        $exp_date = mb_substr($whois_result, $pos + 13, 10);  
        $exp_date_plus_year = strtotime($exp_date . ' +1 year');
        $exp_date = date('Y-m-d', $exp_date_plus_year); 
      }

      // br_region 
      if ($tld == 'br') {
        $pos = strrpos($whois_result, "expires:");			
        $exp_date = mb_substr($whois_result, $pos + 13, 10); 
      }

      // id_region 
      if ($tld == 'id') {
        $pos = strrpos($whois_result, "Expiration Date:");			
        $exp_date = mb_substr($whois_result, $pos + 17, 10);         
      }
      
      $exp_date = date("d-m-Y", strtotime($exp_date));
      
      return $exp_date != '01-01-1970' ? $exp_date : 'x';
    }


}