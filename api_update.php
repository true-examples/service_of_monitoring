<? require './autoload/autoload.php'; ?>

<? 
$whois_api = new WhoisApi($WHOIS_SERVERS);
$db = new DataBase($DB_ACCESS);
$utils = new Utils();


if ($_GET['update'] == 'last-domain') {

	$last_updated_data = $db->getLastUpdatedDomainData();
	$last_updated_domain = $last_updated_data['domain'];
	$last_updated_time = $last_updated_data['updated'];

	$updated_today = $utils->isToday($last_updated_time);

	if (!$updated_today) {		
		$full_domain_report = $whois_api->fullDomainReport($last_updated_domain);
		$db->updateDomainDataByReport($full_domain_report);        
	}

}


if ($_GET['update'] == 'access-error') {
    
  $domains_data = $db->getDomainsDataToCheck('access');
  
	foreach ($domains_data as $domain_data) {
  
		$domain = $domain_data['domain'];
	
		$http_status = $whois_api->getHttpStatus($domain); 
		$size = $whois_api->isValidPageSize ($domain);   

		if (!in_array($http_status, [200, 301, 302]) || !$size) {
			$db->manageAccessError($domain, true, $http_status);    
		} else {			
	  	$db->manageAccessError($domain, false, $http_status);
		}
	} 
	 
}






