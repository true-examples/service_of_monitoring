<? require './autoload/autoload.php'; ?>

<? 

$telegram_bot = new TelegramBot($BOT_TOKEN);
$db = new DataBase($DB_ACCESS);
$utils = new Utils();


if ($_GET['check'] == 'domain') {    
	$telegram_bot->initConnection();

  $domains_data = $db->getDomainsDataToCheck('domain');    
        
    foreach ($domains_data as $domain_data) {            
      $domain_expire_date = $domain_data['domain_expire_date'];

      if ($domain_expire_date != 'x') {
        $days_to_expire = $utils->daysToDate($domain_expire_date);

        if ($days_to_expire <= 7 || in_array($days_to_expire, [14, 21, 28])) {
        }
        $telegram_bot->sendAlertToChats('domain', $domain_data);
				
      }
    }                  
}

if ($_GET['check'] == 'ssl') {
	$telegram_bot->initConnection();
  $domains_data = $db->getDomainsDataToCheck('ssl');    
        
    foreach ($domains_data as $domain_data) {            
      $ssl_expire_date = $domain_data['ssl_expire_date'];

      if ($ssl_expire_date != 'x') {
        $days_to_expire = $utils->daysToDate($ssl_expire_date);

        if ($days_to_expire <= 7 || in_array($days_to_expire, [14, 21, 28])) {
					$telegram_bot->sendAlertToChats('ssl', $domain_data);
				}
								
      }
    } 
}

if ($_GET['check'] == 'access-error') {
  $telegram_bot->initConnection();
  $domains_data = $db->getDomainsDataToCheck('access');
  
	foreach ($domains_data as $domain_data) {
    
    $domain = $domain_data['domain'];
    $access_error_counter = (int)$domain_data['access_error_counter'];
    $access_error_sent = (int)$domain_data['access_error_sent'];
    
    if ($access_error_counter > 2 && $access_error_sent == 0) {    
      $telegram_bot->sendAlertToChats('first-access-error', $domain_data);
      $db->setAlertMessageSent($domain, true);
    }
    
    if (($access_error_counter - 3) % 36 == 0 && $access_error_counter > 3) {    
      $telegram_bot->sendAlertToChats('next-access-error', $domain_data);      
    }

    if ($access_error_counter == 0 && $access_error_sent == 1) {
      $telegram_bot->sendAlertToChats('access-resolve', $domain_data);
      $db->setAlertMessageSent($domain, false);
    }

	}  
}








